package com.school.tejas;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class passwordValidatorTest {

	
	@Test
	public void testCheckPasswordlength() {
		assertTrue("It's good", passwordValidator.checkPasswordlength("hello tejas"));
	}

	@Test
	public void testCheckPasswordlengthException() {
		assertTrue("It's good", passwordValidator.checkPasswordlength("hello   "));
	}

	@Test
	public void testCheckPasswordlengthBoundaryIn() {
		assertTrue("It's good", !passwordValidator.checkPasswordlength("hellotejas"));
	}

	@Test
	public void testCheckPasswordlengthBoundaryOut() {
		assertTrue("It's good", passwordValidator.checkPasswordlength("hellohello"));
	}



	@Test
	public void testCheckDigits() {
		try {
			@SuppressWarnings("unused")
			boolean flag = passwordValidator.checkDigits("ak-47isbest");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test//(expected = Exception.class)
	public void testCheckDigitsException() throws Exception {
		boolean flag = passwordValidator.checkDigits("sheridancollege");

	}

	@Test//(expected = Exception.class)
	public void testCheckDigitsBoundaryIn() throws Exception {
		boolean flag = passwordValidator.checkDigits("sheridancollege");
		assertTrue("It's good", flag);
	}

	@Test
	public void testCheckDigitsBoundaryOut() throws Exception {
		boolean flag = passwordValidator.checkDigits("tejaspatel123");
		assertTrue("It's good", flag);
	}

}
